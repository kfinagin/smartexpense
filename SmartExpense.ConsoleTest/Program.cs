﻿namespace SmartExpense.ConsoleTest
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    class Program
    {
        static void Main(string[] args)
        {
            var ld = new LanguageData()
            {
                Languages = new List<Language>()
                {
                    new Language()
                    {
                        LanguageCode = "Ru",
                        LanguageDescription = "Русский",
                        SystemPhrases = new Dictionary<string, string>()
                        {
                            { "Menu.Add0", "Add" },
                            { "Menu.Add1", "Add" },
                            { "Menu.Add2", "Add" }
                        }
                    },
                    new Language()
                    {
                        LanguageCode = "En",
                        LanguageDescription = "English",
                        SystemPhrases = new Dictionary<string, string>()
                        {
                            { "Menu.Add0", "Add" },
                            { "Menu.Add1", "Add" },
                            { "Menu.Add2", "Add" }
                        }
                    },
                }
            };

            var stringData = JsonConvert.SerializeObject(ld, Formatting.Indented);

            Console.ReadLine();
        }
    }

    class LanguageData
    {
        public List<Language> Languages { get; set; } 
    }

    class Language
    {
        public string LanguageCode { get; set; }
        public string LanguageDescription { get; set; }
        public Dictionary<string, string> SystemPhrases { get; set; }
    }


}
