using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using SmartExpense.Dal.SQLite;

namespace SmartExpense.Dal.Migrations
{
    [DbContext(typeof(SQLiteContext))]
    [Migration("20160116204032_Migration001")]
    partial class Migration001
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348");

            modelBuilder.Entity("SmartExpense.Domain.Classes.Category", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<Guid?>("ParentCategoryId");

                    b.Property<string>("QuickKey");

                    b.Property<long>("UsageCount");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.CategoryTag", b =>
                {
                    b.Property<Guid>("CategoryId");

                    b.Property<Guid>("TagId");

                    b.HasKey("CategoryId", "TagId");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Currency", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("Name");

                    b.Property<string>("Symbol");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Location", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Payment", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount");

                    b.Property<Guid>("CategoryId");

                    b.Property<Guid>("CurrencyId");

                    b.Property<DateTime>("DateTime");

                    b.Property<string>("Description");

                    b.Property<Guid>("LocationId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.PaymentTag", b =>
                {
                    b.Property<Guid>("PaymentId");

                    b.Property<Guid>("TagId");

                    b.HasKey("PaymentId", "TagId");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Tag", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Contents");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Category", b =>
                {
                    b.HasOne("SmartExpense.Domain.Classes.Category")
                        .WithMany()
                        .HasForeignKey("ParentCategoryId");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.CategoryTag", b =>
                {
                    b.HasOne("SmartExpense.Domain.Classes.Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("SmartExpense.Domain.Classes.Tag")
                        .WithMany()
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.Payment", b =>
                {
                    b.HasOne("SmartExpense.Domain.Classes.Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("SmartExpense.Domain.Classes.Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId");

                    b.HasOne("SmartExpense.Domain.Classes.Location")
                        .WithMany()
                        .HasForeignKey("LocationId");
                });

            modelBuilder.Entity("SmartExpense.Domain.Classes.PaymentTag", b =>
                {
                    b.HasOne("SmartExpense.Domain.Classes.Payment")
                        .WithMany()
                        .HasForeignKey("PaymentId");

                    b.HasOne("SmartExpense.Domain.Classes.Tag")
                        .WithMany()
                        .HasForeignKey("TagId");
                });
        }
    }
}
