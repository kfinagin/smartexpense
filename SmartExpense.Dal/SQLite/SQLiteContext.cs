﻿namespace SmartExpense.Dal.SQLite
{
    using Microsoft.Data.Entity;
    using SmartExpense.Domain.Classes;

    public class SQLiteContext : DbContext
    {
        private const string DatabaseFileName = "Payments.db";
        
        public DbSet<Category> Categories { get; set; }
        
        public DbSet<Payment> Payments { get; set; }
        
        public DbSet<Location> Locations { get; set; }
        
        public DbSet<Currency> Currencies { get; set; }  

        public DbSet<CategoryTag> CategoryTags { get; set; }
        
        public DbSet<PaymentTag> PaymentTags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasOne(c => c.ParentCategory)
                .WithMany(c => c.Subcategories)
                .HasForeignKey(c => c.ParentCategoryId);

            modelBuilder.Entity<CategoryTag>()
                .HasOne(ct => ct.Category)
                .WithMany(c => c.CategoryTags)
                .HasForeignKey(ct => ct.CategoryId);

            modelBuilder.Entity<CategoryTag>()
                .HasOne(ct => ct.Tag)
                .WithMany(t => t.CategoryTags)
                .HasForeignKey(ct => ct.TagId);

            modelBuilder.Entity<CategoryTag>()
                .HasKey(ct => new { ct.CategoryId, ct.TagId });

            modelBuilder.Entity<PaymentTag>()
                .HasOne(pt => pt.Payment)
                .WithMany(p => p.PaymentTags)
                .HasForeignKey(pt => pt.PaymentId);

            modelBuilder.Entity<PaymentTag>()
                .HasOne(pt => pt.Tag)
                .WithMany(t => t.PaymentTags)
                .HasForeignKey(pt => pt.TagId);

            modelBuilder.Entity<PaymentTag>()
                .HasKey(pt => new { pt.PaymentId, pt.TagId });

            modelBuilder.Entity<Payment>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Payments)
                .HasForeignKey(p => p.CategoryId);

            modelBuilder.Entity<Payment>()
                .HasOne(p => p.Currency)
                .WithMany(c => c.Payments)
                .HasForeignKey(p => p.CurrencyId);

            modelBuilder.Entity<Payment>()
                .HasOne(p => p.Location)
                .WithMany(l => l.Payments)
                .HasForeignKey(p => p.LocationId);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"FileName=" + DatabaseFileName);
        }
    }
}
