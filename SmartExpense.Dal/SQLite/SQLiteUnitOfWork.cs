﻿namespace SmartExpense.Dal.SQLite
{
    using System;
    using System.Threading.Tasks;
    using SmartExpense.Domain.Classes;

    public class SQLiteUnitOfWork : IUnitOfWork
    {
        private readonly SQLiteContext context;

        private IRepository<Category> categoriesRepository;
        private IRepository<Payment> paymentRepository;
        private IRepository<Currency> currencyRepository;
        private IRepository<Location> locationRepository;
        
        public SQLiteUnitOfWork()
        {
            this.context = new SQLiteContext();    
        }

        ~SQLiteUnitOfWork()
        {
            this.Dispose();   
        }
        
        public IRepository<Category> CategoriesRepository => 
            this.categoriesRepository ?? (this.categoriesRepository = new SQLiteRepository<Category>(this.context));

        public IRepository<Payment> PaymentRepository =>
            this.paymentRepository ?? (this.paymentRepository = new SQLiteRepository<Payment>(this.context));
        
        public IRepository<Currency> CurrencyRepository =>
            this.currencyRepository ?? (this.currencyRepository = new SQLiteRepository<Currency>(this.context));

        public IRepository<Location> LocationRepository =>
            this.locationRepository ?? (this.locationRepository = new SQLiteRepository<Location>(this.context));

        public async Task SaveChangesAsync()
        {
            await this.context.SaveChangesAsync();
        }
        public void Dispose()
        {
            this.context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
