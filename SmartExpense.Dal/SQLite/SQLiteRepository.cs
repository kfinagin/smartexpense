﻿using Windows.UI.Xaml.Documents;
using Microsoft.Data.Entity;

namespace SmartExpense.Dal.SQLite
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using SmartExpense.Domain;

    public class SQLiteRepository<T> : IRepository<T> where T : class, IGuidIdentifiableEntity
    {
        private readonly DbSet<T> dbSet; 

        public SQLiteRepository(SQLiteContext context)
        {
            this.dbSet = context.Set<T>();
        }

        public void Add(T newEntity)
        {
            this.dbSet.Add(newEntity);
        }

        public void Delete(T newEntity)
        {
            this.dbSet.Remove(newEntity);
        }

        public void Update(T updatedEntity, bool includeDependents = false)
        {
            var graphBehavior = includeDependents 
                ? GraphBehavior.IncludeDependents 
                : GraphBehavior.SingleObject;

            this.dbSet.Update(updatedEntity, graphBehavior);
        }
        
        public T FindById(Guid id)
        {
            return this.dbSet.FirstOrDefault(i => i.Id == id);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return this.dbSet.Where(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return this.dbSet;
        }
    }
}
