﻿namespace SmartExpense.Dal
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using SmartExpense.Domain;

    public interface IRepository<T> where T: IGuidIdentifiableEntity
    {
        void Add(T newEntity);

        void Delete(T newEntity);

        void Update(T updatedEntity, bool includeDependents = false);

        T FindById(Guid id);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll();
    }
}
