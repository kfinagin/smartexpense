﻿using System;

namespace SmartExpense.Dal
{
    using System.Threading.Tasks;
    using SmartExpense.Domain.Classes;

    public interface IUnitOfWork : IDisposable
    {
        IRepository<Category> CategoriesRepository { get; }

        IRepository<Payment> PaymentRepository { get; }

        IRepository<Currency> CurrencyRepository { get; }

        IRepository<Location> LocationRepository { get; }

        Task SaveChangesAsync();
    }
}
