﻿namespace SmartExpense.UILogic.Services.Translation
{
    using System.Threading.Tasks;

    public interface IContentTranslationService
    {
        void SwitchLanguage(string code);

        string this[string index] { get; }
    }
}
