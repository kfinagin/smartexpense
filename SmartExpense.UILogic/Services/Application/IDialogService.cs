﻿namespace SmartExpense.UILogic.Services.Application
{
    public interface IDialogService
    {
        void ShowMessage(string message);
    }
}
