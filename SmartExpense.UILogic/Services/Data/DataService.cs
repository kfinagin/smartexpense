﻿namespace SmartExpense.UILogic.Services.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Data.Entity;
    using SmartExpense.Dal;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.Factories;

    /// <summary>
    ///     Creating domain objects from viewmodel data and obtaining viewmodel objects from the data access layer
    /// </summary>
    public class DataService : IDataService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DataService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task AddCategoryAsync(
            string name, 
            string description,
            string tags,
            Category parentCategory
            )
        {
            var category = new Category()
            {
                Name = name,
                Description = description, 
                ParentCategoryId = parentCategory?.Id
            };

            using (var uow = this.unitOfWorkFactory())
            {
                uow.CategoriesRepository.Add(category);
                await uow.SaveChangesAsync();
            }

            await AddTags(tags, category.Id);
        }

        private async Task AddTags(string tags, Guid categoryId)
        {
            
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            using (var uow = this.unitOfWorkFactory())
            {
                var categories = uow.CategoriesRepository.Find(c => c.ParentCategory == null);
                return await categories.ToListAsync();
            }
        }

        public async Task<IEnumerable<Category>> GetSubcategoriesAsync(Guid parentCategoryId)
        {
            using (var uow = this.unitOfWorkFactory())
            {
                var subcategories = uow.CategoriesRepository.Find(c => c.ParentCategoryId == parentCategoryId);
                return await subcategories.ToListAsync();
            }
        }

        public async Task DeleteCategory(Category selectedCategory)
        {
            using (var uow = this.unitOfWorkFactory())
            {
                uow.CategoriesRepository.Delete(selectedCategory);
                await uow.SaveChangesAsync();
            }

        }

        public async Task CheckDefaultCurrenciesAsync()
        {
            using (var uow = this.unitOfWorkFactory())
            {
                var currencies = uow.CurrencyRepository.GetAll();
                
                // adding default currencies
                if (currencies.FirstOrDefault(c => c.Code == "RUR") == null)
                {
                    uow.CurrencyRepository.Add(CurrencyFactory.GetRuble());
                }
                if (currencies.FirstOrDefault(c => c.Code == "EUR") == null)
                {
                    uow.CurrencyRepository.Add(CurrencyFactory.GetEuro());
                }
                if (currencies.FirstOrDefault(c => c.Code == "USD") == null)
                {
                    uow.CurrencyRepository.Add(CurrencyFactory.GetUsd());
                }

                await uow.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Currency>> GetCurrenciesAsync()
        {
            using (var uow = this.unitOfWorkFactory())
            {
                return await uow.CurrencyRepository.GetAll().ToListAsync();
            }
        }
    }
}
