﻿namespace SmartExpense.UILogic.Services.Data
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SmartExpense.Domain.Classes;

    public interface IDataService
    {
        Task AddCategoryAsync(string name, string description, string tags, Category parentCategory);

        Task<IEnumerable<Category>> GetCategoriesAsync();

        Task<IEnumerable<Category>> GetSubcategoriesAsync(Guid parentCategoryId);

        Task DeleteCategory(Category selectedCategory);

        Task CheckDefaultCurrenciesAsync();

        Task<IEnumerable<Currency>> GetCurrenciesAsync();
    }
}
