﻿namespace SmartExpense.UILogic.Enums
{
    public enum NavigationTarget
    {
        AddCategory,

        EditCategory
    }
}
