﻿namespace SmartExpense.UILogic.Factories
{
    using SmartExpense.Domain.Classes;

    public static class CurrencyFactory
    {
        public static Currency GetRuble()
        {
            return new Currency()
            {
                Name = "Russian ruble",
                Code = "RUR",
                Symbol = "₽"
            };
        }

        public static Currency GetUsd()
        {
            return new Currency()
            {
                Name = "United states dollar",
                Code = "USD",
                Symbol = "$"
            };
        }

        public static Currency GetEuro()
        {
            return new Currency()
            {
                Name = "Euro",
                Code = "EUR",
                Symbol = "€"
            };
        }
    }
}
