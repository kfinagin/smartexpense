﻿namespace SmartExpense.UILogic.Factories
{
    using System;
    using System.Collections;
    using SmartExpense.UILogic.ControlClasses;
    using System.Collections.Generic;

    public interface IMenuItemFactory
    {
        MenuItem GetAddChequeMenuItem();
        MenuItem GetSettingsMenuItem();
        MenuItem GetHistoryMenuItem();
        MenuItem GetStatisticsMenuItem();

        IEnumerable<MenuItem> GetAllMenuItems();
    }
}
