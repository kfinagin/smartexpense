﻿namespace SmartExpense.UILogic.Factories
{
    using System;
    using System.Collections.Generic;
    using SmartExpense.UILogic.ControlClasses;
    using SmartExpense.UILogic.Services;
    using SmartExpense.UILogic.Services.Translation;
    using SmartExpense.UILogic.ViewModels;

    public class MenuItemFactory : IMenuItemFactory
    {
        private readonly IContentTranslationService contentTranslationService;

        public MenuItemFactory(IContentTranslationService contentTranslationService)
        {
            this.contentTranslationService = contentTranslationService;
        }

        public MenuItem GetAddChequeMenuItem()
        {
            return new MenuItem()
            {
                Description = this.contentTranslationService["Menu.Add"],
                IconSymbol = "\uE710",
                ViewModelType = ViewModelType.AddCheque
            };
        }

        public MenuItem GetSettingsMenuItem()
        {
            return new MenuItem()
            {
                Description = this.contentTranslationService["Menu.Settings"],
                IconSymbol = "\uE713",
                ViewModelType = ViewModelType.Settings
            };
        }

        public MenuItem GetHistoryMenuItem()
        {
            return new MenuItem()
            {
                Description = this.contentTranslationService["Menu.History"],
                IconSymbol = "\uE787",
                ViewModelType = ViewModelType.History
            };

        }

        public MenuItem GetStatisticsMenuItem()
        {
            return new MenuItem()
            {
                Description = this.contentTranslationService["Menu.Statistics"],
                IconPath = "F1 M 6.66,7.9696L 0,7.9696L 0,28.74L 6.66,28.74L 6.66,7.9696 Z M 16.26,13.283L 9.60004,13.283L 9.60004,28.74L 16.26,28.74L 16.26,13.283 Z M 25.86,-3.05176e-005L 19.2,-3.05176e-005L 19.2,28.74L 25.86,28.74L 25.86,-3.05176e-005 Z ",
                ViewModelType = ViewModelType.Statistics
            };
        }

        public IEnumerable<MenuItem> GetAllMenuItems()
        {
            return new List<MenuItem>()
            {
                GetAddChequeMenuItem(),
                GetHistoryMenuItem(),
                GetStatisticsMenuItem(),
                GetSettingsMenuItem()
            };

        }


    }
}
