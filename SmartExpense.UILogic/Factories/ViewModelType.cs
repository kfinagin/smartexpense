﻿namespace SmartExpense.UILogic.Factories
{
    public enum ViewModelType
    {
        AddCheque,
        Settings,
        History,
        Statistics
    }
}