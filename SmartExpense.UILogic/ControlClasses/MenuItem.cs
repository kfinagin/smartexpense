﻿namespace SmartExpense.UILogic.ControlClasses
{
    using System;
    using PropertyChanged;
    using SmartExpense.UILogic.Factories;

    [ImplementPropertyChanged]
    public class MenuItem
    {
        public bool IsSelected { get; set; }

        public string IconSymbol { get; set; }

        public string IconPath { get; set; }

        public string Description { get; set; }

        public Action ClickAction { get; set; }

        public ViewModelType ViewModelType { get; set; }
    }
}
