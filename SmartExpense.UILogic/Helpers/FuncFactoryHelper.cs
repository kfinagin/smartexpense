﻿namespace SmartExpense.UILogic.Helpers
{
    using System;
    using Microsoft.Practices.Unity;

    public static class FuncFactoryHelper
    {
        public static TResult CallWithUnityParametersOverride<TParam1, TResult>(
            this Func<ResolverOverride[], TResult> wrappedFunc, TParam1 param1)
        {
            var result = wrappedFunc(new ResolverOverride[] { new DependencyOverride<TParam1>(param1) });
            return result;
        }

        public static TResult CallWithUnityParametersOverride<TParam1, TParam2, TResult>(
            this Func<ResolverOverride[], TResult> wrappedFunc, TParam1 param1, TParam2 param2)
        {
            var result =
                wrappedFunc(new ResolverOverride[]
                {
                    new DependencyOverride<TParam1>(param1),
                    new DependencyOverride<TParam2>(param2)
                });
            return result;
        }

        public static TResult CallWithUnityParametersOverride<TParam1, TParam2, TParam3, TResult>(
            this Func<ResolverOverride[], TResult> wrappedFunc, TParam1 param1, TParam2 param2, TParam3 param3)
        {
            var result =
                wrappedFunc(new ResolverOverride[]
                {
                    new DependencyOverride<TParam1>(param1),
                    new DependencyOverride<TParam2>(param2),
                    new DependencyOverride<TParam3>(param3),
                });
            return result;
        }
    }
}
