﻿namespace SmartExpense.UILogic.Events
{
    using SmartExpense.UILogic.Enums;

    public class NavigationEvent
    {
        public NavigationTarget NavigationTarget { get; set; }

        public object NavigationParameter { get; set; }
    }
}
