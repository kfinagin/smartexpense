﻿namespace SmartExpense.UILogic.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Caliburn.Micro;
    using Microsoft.Practices.ObjectBuilder2;
    using PropertyChanged;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.ControlClasses;
    using SmartExpense.UILogic.Factories;
    using SmartExpense.UILogic.Services.Application;
    using SmartExpense.UILogic.Services.Data;
    using SmartExpense.UILogic.Services.Translation;

    [ImplementPropertyChanged]
    public partial class ShellViewModel : Conductor<IScreen>.Collection.AllActive
    {
        private readonly IMenuItemFactory menuItemFactory;
        private readonly IEventAggregator eventAggregator;
        private readonly IDialogService dialogService;
        private readonly IContentTranslationService contentService;
        private readonly IDataService dataService;
        private readonly Func<AddChequeViewModel> addChequeViewModelFactory;
        private readonly Func<HistoryViewModel> historyViewModelFactory;
        private readonly Func<StatisticsViewModel> statisticsViewModelFactory;
        private readonly Func<SettingsViewModel> settingsViewModelFactory;
        private readonly Func<AddCategoryViewModel> addCategoryViewModelFactory;

        public ShellViewModel(
            IMenuItemFactory menuItemFactory, 
            IEventAggregator eventAggregator, 
            IDialogService dialogService,
            IContentTranslationService contentService,
            IDataService dataService,
            Func<AddChequeViewModel> addChequeViewModelFactory,
            Func<HistoryViewModel> historyViewModelFactory,
            Func<StatisticsViewModel> statisticsViewModelFactory,
            Func<SettingsViewModel> settingsViewModelFactory,
            Func<AddCategoryViewModel> addCategoryViewModelFactory 
            )
        {
            this.menuItemFactory = menuItemFactory;
            this.eventAggregator = eventAggregator;
            this.dialogService = dialogService;
            this.contentService = contentService;
            this.dataService = dataService;
            this.addChequeViewModelFactory = addChequeViewModelFactory;
            this.historyViewModelFactory = historyViewModelFactory;
            this.statisticsViewModelFactory = statisticsViewModelFactory;
            this.settingsViewModelFactory = settingsViewModelFactory;
            this.addCategoryViewModelFactory = addCategoryViewModelFactory;

            this.MenuItems = new ObservableCollection<MenuItem>();
            this.IsMenuExpanded = false;

            this.eventAggregator.Subscribe(this);

            this.contentService.SwitchLanguage("Ru");
        }

        protected override async void OnActivate()
        {
            ConfigureMenuItems();
            Load(this.MenuItems[0]);

            await this.dataService.CheckDefaultCurrenciesAsync();
        }

        public IScreen ActiveItem { get; set; }
        
        public bool IsMenuExpanded { get; set; }

        public ObservableCollection<MenuItem> MenuItems { get; set; } 

        public void ExpandMenuButtonClicked()
        {
            IsMenuExpanded = !IsMenuExpanded;
        }

        public void MenuButtonClicked(MenuItem menuItem)
        {
            Load(menuItem);
        }
        
        private void ConfigureMenuItems()
        {
            this.MenuItems.Clear();
            this.menuItemFactory.GetAllMenuItems().ForEach(this.MenuItems.Add);
        }

        private void Load(MenuItem item)
        {
            this.MenuItems.Where(i => i.IsSelected).ForEach(i => i.IsSelected = false);
            item.IsSelected = true;

            switch (item.ViewModelType)
            {
                case ViewModelType.AddCheque:
                    ActiveItem = this.addChequeViewModelFactory();
                    break;
                case ViewModelType.Settings:
                    ActiveItem = this.settingsViewModelFactory();
                    break;
                case ViewModelType.History:
                    ActiveItem = this.historyViewModelFactory();
                    break;
                case ViewModelType.Statistics:
                    this.ActiveItem = this.statisticsViewModelFactory();
                    break;
            }
        }
    }
}
