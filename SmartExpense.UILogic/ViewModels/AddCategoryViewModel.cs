﻿namespace SmartExpense.UILogic.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Caliburn.Micro;
    using PropertyChanged;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.Events;
    using SmartExpense.UILogic.Services.Application;
    using SmartExpense.UILogic.Services.Data;

    [ImplementPropertyChanged]
    public sealed class AddCategoryViewModel : Screen
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IDataService dataService;

        public AddCategoryViewModel(
            Category parentCategory,
            IEventAggregator eventAggregator,
            IDataService dataService
            )
        {
            ParentCategory = parentCategory;

            this.eventAggregator = eventAggregator;
            this.dataService = dataService;
            this.DisplayName = "AddCategoryViewModel";
        }

        public bool AddingCategory { get; set; }

        public Category ParentCategory { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Tags { get; set; }

        public async Task AddCategory()
        {
            try
            {
                this.AddingCategory = true;
                await this.dataService.AddCategoryAsync(Name, Description, Tags, ParentCategory);
            }
            catch (Exception)
            {
                //TODO 
            }
            finally
            {
                this.AddingCategory = false;
                this.eventAggregator.PublishOnUIThread(new UpdateCategoriesEvent());
                this.eventAggregator.PublishOnUIThread(new CloseOverlayPanelEvent());
            }
        }
    }
}
