﻿namespace SmartExpense.UILogic.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Caliburn.Micro;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.Enums;
    using SmartExpense.UILogic.Events;

    partial class ShellViewModel :
        IHandle<CloseOverlayPanelEvent>,
        IHandle<DisplayOverlayPanelEvent>
    {
        public IScreen OverlayViewModel { get; set; }

        public bool OverlayIsExpanded { get; set; }

        public async void CloseOverlayPanel()
        {
            await CloseOverlay();
        }

        public async void Handle(CloseOverlayPanelEvent message)
        {
            await CloseOverlay();
        }

        public async void Handle(DisplayOverlayPanelEvent message)
        {
            await OverlayTo(message.NavigationTarget, message.NavigationParameter);
        }

        private async Task CloseOverlay()
        {
            if (OverlayIsExpanded)
            {
                this.OverlayViewModel = null;
                this.OverlayIsExpanded = false;
            }
        }

        private async Task OverlayTo(NavigationTarget target, object parameter)
        {
            switch (target)
            {
                case NavigationTarget.AddCategory:
                    await ShowAddCategory(parameter as Category);
                    break;
                case NavigationTarget.EditCategory:
                    await ShowAddCategory(parameter as Category);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();

            }
        }

        private async Task ShowAddCategory(Category category)
        {
            var categoryViewModel = this.addCategoryViewModelFactory();
            categoryViewModel.ParentCategory = category;

            await ShowOverlay(() => categoryViewModel);
        }

        private async Task ShowOverlay(Func<Task<IScreen>> getViewModel)
        {
            await OverlayPageWithDelegate(getViewModel);
        }

        private async Task ShowOverlay(Func<IScreen> getViewModel)
        {
            await OverlayPageWithDelegate(getViewModel);
        }

        private async Task OverlayPageWithDelegate(object getViewModelDelegate)
        {
            try
            {
                await CloseOverlay();

                OverlayViewModel = await GetViewModelWithDelegate(getViewModelDelegate);

                ActivateItem(OverlayViewModel);

                OverlayIsExpanded = true;
            }
            catch (Exception ex)
            {
                this.dialogService.ShowMessage("Exception occured: " + ex.Message);
            }
            finally
            {
                // TODO progress ring display
            }
        }

        private static async Task<IScreen> GetViewModelWithDelegate(object getViewModelDelegate)
        {
            IScreen vm = null;

            var getViewModelAsync = getViewModelDelegate as Func<Task<IScreen>>;
            if (getViewModelAsync != null)
            {
                vm = await getViewModelAsync();
            }

            var getViewModel = getViewModelDelegate as Func<IScreen>;
            if (getViewModel != null)
            {
                vm = getViewModel();
            }

            return vm;
        }
    }
}










