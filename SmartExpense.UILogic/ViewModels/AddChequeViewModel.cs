﻿namespace SmartExpense.UILogic.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using Caliburn.Micro;
    using PropertyChanged;
    using SmartExpense.Dal.SQLite;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.Enums;
    using SmartExpense.UILogic.Events;
    using SmartExpense.UILogic.Services.Application;
    using SmartExpense.UILogic.Services.Data;
    using SmartExpense.UILogic.Services.Translation;

    [ImplementPropertyChanged]
    public class AddChequeViewModel : Screen, IHandle<UpdateCategoriesEvent>
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;

        public AddChequeViewModel(
            IEventAggregator eventAggregator, 
            IDataService dataService,
            IDialogService dialogService,
            IContentTranslationService contentService
            )
        {
            this.eventAggregator = eventAggregator;
            this.dataService = dataService;
            this.dialogService = dialogService;

            this.Categories = new ObservableCollection<Category>();
            this.Subcategories = new ObservableCollection<Category>();
            this.Currencies = new ObservableCollection<Currency>();

            this.ContentService = contentService;

            this.eventAggregator.Subscribe(this);
        }

        protected override async void OnViewLoaded(object view)
        {
            await UpdateCategories();
            await UpdateCurrencies();
        }

        public IContentTranslationService ContentService { get; private set; }

        public ObservableCollection<Category> Categories { get; set; }

        public Category SelectedCategory { get; set; }

        public ObservableCollection<Category> Subcategories { get; set; }

        public Category SelectedSubcategory { get; set; }

        public ObservableCollection<Payment> LastPayments { get; set; } 

        public ObservableCollection<Currency> Currencies { get; set; } 

        public Currency SelectedCurrency { get; set; }

        public async Task UpdateCategories()
        {
            this.Categories.Clear();

            var categories = await this.dataService.GetCategoriesAsync();

            foreach (var category in categories)
            {
                this.Categories.Add(category);
            }

            if (this.Categories.Count > 0)
            {
                this.SelectedCategory = this.Categories[0];
            }
        }

        public async Task UpdateSubcategories()
        {
            this.Subcategories.Clear();

            var subcategories = await this.dataService.GetSubcategoriesAsync(SelectedCategory.Id);

            foreach (var subcategory in subcategories)
            {
                this.Subcategories.Add(subcategory);
            }

            if (this.Subcategories.Count > 0)
            {
                this.SelectedSubcategory = this.Subcategories[0];
            }
        }

        public async Task UpdateCurrencies()
        {
            this.Currencies.Clear();
            var currencies = await this.dataService.GetCurrenciesAsync();

            foreach (var currency in currencies)
            {
                this.Currencies.Add(currency);
            }

            if (this.Currencies.Count > 0)
            {
                this.SelectedCurrency = this.Currencies[0];
            }
        }

        public void AddMainCategory()
        {
            this.eventAggregator.PublishOnUIThread(new DisplayOverlayPanelEvent()
            {
                NavigationTarget = NavigationTarget.AddCategory,
                NavigationParameter = null
            });             
        }

        public void EditMainCategory()
        {

        }

        public async Task DeleteMainCategory()
        {
            await this.dataService.DeleteCategory(SelectedCategory);
            await UpdateCategories();
        }

        public void AddSubcategory()
        {
            this.eventAggregator.PublishOnUIThread(new DisplayOverlayPanelEvent()
            {
                NavigationTarget = NavigationTarget.AddCategory,
                NavigationParameter = SelectedCategory
            });
        }

        public void EditSubcategory()
        {
            
        }

        public async Task DeleteSubcategory()
        {
            await this.dataService.DeleteCategory(SelectedSubcategory);
            await UpdateSubcategories();
        }

        public async Task AcceptPayment()
        {
            




        }
        
        public async void Handle(UpdateCategoriesEvent message)
        {
            await this.UpdateCategories();
        }
    }
}

