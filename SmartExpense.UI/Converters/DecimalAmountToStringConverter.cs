﻿namespace SmartExpense.UI.Converters
{
    using System;
    using System.Globalization;
    using Windows.UI.Xaml.Data;
    using SmartExpense.Domain.Classes;

    public class DecimalAmountToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var amount = (decimal) value;
            return amount.ToString("N2", CultureInfo.CurrentCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new System.NotImplementedException();
        }
    }
}
