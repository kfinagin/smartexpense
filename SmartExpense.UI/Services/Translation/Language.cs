namespace SmartExpense.UI.Services.Translation
{
    using System.Collections.Generic;

    internal class Language
    {
        public string LanguageCode { get; set; }
        public string LanguageDescription { get; set; }

        public Dictionary<string, string> SystemPhrases { get; set; }
    }
}