namespace SmartExpense.UI.Services.Translation
{
    using System.Collections.Generic;

    internal class LanguageData
    {
        public List<Language> Languages { get; set; }
    }
}