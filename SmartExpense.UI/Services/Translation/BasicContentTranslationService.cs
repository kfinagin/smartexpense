﻿namespace SmartExpense.UI.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Windows.Storage;
    using Caliburn.Micro;
    using Newtonsoft.Json;
    using SmartExpense.UI.Services.Translation;
    using SmartExpense.UILogic.Events;
    using SmartExpense.UILogic.Services;
    using SmartExpense.UILogic.Services.Translation;

    /// <summary>
    ///     Simple content translations service that uses translations from the application resources
    /// </summary>
    public class BasicContentTranslationService : IContentTranslationService
    {
        private readonly IEventAggregator eventAggregator;
        
        private LanguageData languageData;
        private Language currentLanguage;

        public BasicContentTranslationService(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            ReadLanguageDataFile();
        }

        public void SwitchLanguage(string code)
        {
            this.currentLanguage = this.languageData.Languages
                .First(lang => string.Equals(lang.LanguageCode, code));
                
            this.eventAggregator.PublishOnUIThread(new LanguageChangedEvent());
        }
        
        public string this[string index] => GetContentData(index);

        private void ReadLanguageDataFile()
        {
            var currentAssetsFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            string languageListFileName = @"Assets\Translations\LanguageData.json";
            var languageListFile = currentAssetsFolder.GetFileAsync(languageListFileName)
                .GetAwaiter().GetResult();

            string fileContents = FileIO.ReadTextAsync(languageListFile)
                .GetAwaiter().GetResult();

            this.languageData = JsonConvert.DeserializeObject<LanguageData>(fileContents);
            this.currentLanguage = this.languageData.Languages[0];
        }

        private string GetContentData(string index)
        {
            string translation;
            var result = this.currentLanguage.SystemPhrases.TryGetValue(index, out translation);

            if (result)
            {
                return translation.Trim();
            }

            return GetStringDataFromIndex(index);
        }

        private string GetStringDataFromIndex(string index)
        {
            var parts = index.Split('.');
            var lastPart = parts[parts.Length - 1];
            var result = lastPart.Replace('_', ' ');
            return result.Trim();
        }

    }
}
