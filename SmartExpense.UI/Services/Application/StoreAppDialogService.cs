﻿namespace SmartExpense.UI.Services.Application
{
    using Windows.UI.Popups;
    using SmartExpense.UILogic.Services.Application;

    public class StoreAppDialogService : IDialogService
    {
        public void ShowMessage(string message)
        {
            var dlg = new MessageDialog(message);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            dlg.ShowAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
    }
}
