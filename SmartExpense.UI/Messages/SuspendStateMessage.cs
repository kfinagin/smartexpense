﻿namespace SmartExpense.UI.Messages
{
    using Windows.ApplicationModel;

    public class SuspendStateMessage
    {
        public SuspendStateMessage(SuspendingOperation operation)
        {
            Operation = operation;
        }

        public SuspendingOperation Operation { get; }
    }
}
