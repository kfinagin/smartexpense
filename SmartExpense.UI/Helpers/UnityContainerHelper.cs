﻿namespace SmartExpense.UI.Helpers
{
    using System;
    using Microsoft.Practices.Unity;

    public static class UnityContainerHelper
    {
        public static void RegisterContainerControlled<TInterface, TInstance>(this UnityContainer container)
            where TInstance : TInterface
        {
            container.RegisterType<TInterface, TInstance>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterContainerControlled<TInstance>(this UnityContainer container)
        {
            container.RegisterType<TInstance>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterPerResolve<TInterface, TInstance>(this UnityContainer container)
            where TInstance : TInterface
        {
            container.RegisterType<TInterface, TInstance>(new PerResolveLifetimeManager());
        }

        public static void RegisterPerResolve<TInstance>(this UnityContainer container)
        {
            container.RegisterType<TInstance>(new PerResolveLifetimeManager());
        }

        public static void RegisterFuncBuilder<TInterface, TInstance>(this UnityContainer container, bool isSingleton) 
            where TInstance : TInterface
        {
            if (isSingleton)
            {
                container.RegisterContainerControlled<TInterface, TInstance>();
            }
            else
            {
                container.RegisterPerResolve<TInterface, TInstance>();
            }

            container.RegisterType<Func<TInterface>>(
                new InjectionFactory(i => new Func<TInterface>(() => container.Resolve<TInterface>())));
        }

        public static void RegisterFuncBuilder<TInstance>(this UnityContainer container, bool isSingleton)
        {
            if (isSingleton)
            {
                container.RegisterContainerControlled<TInstance>();
            }
            else
            {
                container.RegisterPerResolve<TInstance>();
            }

            container.RegisterType<Func<TInstance>>(
                new InjectionFactory(i => new Func<TInstance>(() => container.Resolve<TInstance>())));
        }
    }
}
