﻿using Microsoft.Data.Entity;
using SmartExpense.Dal;
using SmartExpense.Dal.SQLite;

namespace SmartExpense.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Windows.ApplicationModel;
    using Windows.ApplicationModel.Activation;
    using Windows.UI;
    using Windows.UI.ViewManagement;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Media;
    using Caliburn.Micro;
    using Microsoft.Practices.Unity;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UI.Helpers;
    using SmartExpense.UI.Messages;
    using SmartExpense.UI.Services;
    using SmartExpense.UI.Services.Application;
    using SmartExpense.UILogic.Factories;
    using SmartExpense.UILogic.Services;
    using SmartExpense.UILogic.Services.Application;
    using SmartExpense.UILogic.Services.Data;
    using SmartExpense.UILogic.Services.Translation;
    using SmartExpense.UILogic.ViewModels;

    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : CaliburnApplication
    {
        private Assembly viewAssembly;
        private UnityContainer container;

        private IEventAggregator eventAggregator;

        public App()
        {
            InitializeComponent();
        }

        #region Component Registration

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            var assemblies = base.SelectAssemblies().ToList();
            this.viewAssembly = assemblies.First();

            assemblies.Add(typeof (ShellViewModel).GetTypeInfo().Assembly);
            return assemblies;
        }
        
        protected override void Configure()
        {
            ConfigureTypeMappings();

            this.container = new UnityContainer();

            MigrateDatabase();

            RegisterServices();
            RegisterViewModels();

            this.eventAggregator = this.container.Resolve<IEventAggregator>();
        }
        
        private void ConfigureTypeMappings()
        {
            ViewLocator.LocateForModelType = (viewModelType, visualParent, context) =>
            {
                string viewTypeName = viewModelType.FullName
                    .Replace("UILogic.ViewModels", "UI")
                    .Replace("Model", string.Empty);

                var viewType = this.viewAssembly.GetViewType(viewTypeName);
                var view = ViewLocator.GetOrCreateViewType(viewType);

                return view;
            };

        }

        private void RegisterServices()
        {
            this.container.RegisterContainerControlled<IEventAggregator, EventAggregator>();
            this.container.RegisterContainerControlled<IDialogService, StoreAppDialogService>();
            this.container.RegisterContainerControlled<IContentTranslationService, BasicContentTranslationService>();

            this.container.RegisterContainerControlled<IDataService, DataService>();
            
            this.container.RegisterFuncBuilder<IUnitOfWork, SQLiteUnitOfWork>(false);
        }
        
        private void RegisterViewModels()
        {
            this.container.RegisterPerResolve<ShellViewModel>();

            this.container.RegisterContainerControlled<IMenuItemFactory, MenuItemFactory>();

            this.container.RegisterFuncBuilder<AddChequeViewModel>(true);
            this.container.RegisterFuncBuilder<StatisticsViewModel>(true);
            this.container.RegisterFuncBuilder<HistoryViewModel>(true);
            this.container.RegisterFuncBuilder<SettingsViewModel>(true);

            this.container.RegisterFuncBuilder<AddCategoryViewModel>(false);
            
        }

        private void MigrateDatabase()
        {
            using (var context = new SQLiteContext())
            {
                context.Database.Migrate();
            }
        }

        #endregion Component Registration

        #region Overrides

        protected override object GetInstance(Type service, string key)
        {
            if (service != null)
            {
                return this.container.Resolve(service);
            }

            if (!string.IsNullOrWhiteSpace(key))
            {
                return this.container.Resolve(Type.GetType(key));
            }

            return null;
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return this.container.ResolveAll(service);
        }

        protected override void BuildUp(object instance)
        {
            this.container.BuildUp(instance);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            DisplayRootViewFor<ShellViewModel>();

            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                this.eventAggregator.PublishOnUIThread(new ResumeStateMessage());   
            }
        }

        protected override void OnSuspending(object sender, SuspendingEventArgs e)
        {
            this.eventAggregator.PublishOnUIThread(new SuspendStateMessage(e.SuspendingOperation));
        }

        #endregion Overrides

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            var titleBar = ApplicationView.GetForCurrentView().TitleBar;

            // set up our brushes
            SolidColorBrush backgroundColor = Current.Resources["TitleBarBackgroundThemeBrush"] as SolidColorBrush;
            SolidColorBrush buttonHoverColor = Current.Resources["TitleBarButtonHoverThemeBrush"] as SolidColorBrush;
            SolidColorBrush buttonPressedColor = Current.Resources["TitleBarButtonPressedThemeBrush"] as SolidColorBrush;

            // override colors!
            titleBar.BackgroundColor = backgroundColor?.Color;
            titleBar.ForegroundColor = Colors.White;
            titleBar.ButtonBackgroundColor = backgroundColor?.Color;
            titleBar.ButtonForegroundColor = Colors.White;
            titleBar.ButtonHoverBackgroundColor = buttonHoverColor?.Color;
            titleBar.ButtonHoverForegroundColor = Colors.White;
            titleBar.ButtonPressedBackgroundColor = buttonPressedColor?.Color;
            titleBar.ButtonPressedForegroundColor = Colors.White;
            titleBar.InactiveBackgroundColor = backgroundColor?.Color;
            titleBar.InactiveForegroundColor = Colors.White;
            titleBar.ButtonInactiveBackgroundColor = backgroundColor?.Color;
            titleBar.ButtonInactiveForegroundColor = Colors.White;
        }
    }
}
