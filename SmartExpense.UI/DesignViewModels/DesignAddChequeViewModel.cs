﻿namespace SmartExpense.UI.DesignViewModels
{
    using System;
    using System.Collections.Generic;
    using SmartExpense.Domain.Classes;
    using SmartExpense.UILogic.Services.Translation;

    public class DesignAddChequeViewModel
    {
        public IContentTranslationService ContentService { get; set; }

        public DesignAddChequeViewModel()
        {
            this.ContentService = new DummyTranslationService();

            this.Categories = new List<Category>()
            {
                GetTestCategory(),
                GetTestCategory(),
                GetTestCategory(),
                GetTestCategory(),
            };

            this.Subcategories = new List<Category>()
            {
                GetTestCategory(),
                GetTestCategory(),
                GetTestCategory(),
                GetTestCategory(),
                GetTestCategory(),
            };

            this.LastPayments = new List<Payment>()
            {
                GetTestPayment(),
                GetTestPayment(),
                GetTestPayment(),
                GetTestPayment(),
                GetTestPayment()
            };
        }

        public List<Category> Categories { get; set; }

        public List<Category> Subcategories { get; set; }

        public List<Payment> LastPayments { get; set; } 

        public Category SelectedCategory { get; set; }

        public Category SelectedSubcategory { get; set; }

        private Payment GetTestPayment()
        {
            return new Payment()
            {
                DateTime = DateTime.Now,
                Amount = 658999.9M,
                Currency = new Currency() { Code="RUR", Symbol = "₽" },
                Category = new Category() { Name = "TestCategory"}
            };
        }

        private Category GetTestCategory()
        {
            return new Category()
            {
                Name = "Test name",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            };
        }
    }
}
