﻿namespace SmartExpense.UI.DesignViewModels
{
    using SmartExpense.UILogic.Services.Translation;

    public class DummyTranslationService : IContentTranslationService
    {
        public void SwitchLanguage(string code)
        {
            

        }

        public string this[string index] => index;
    }
}
