﻿namespace SmartExpense.Domain
{
    using System;

    public interface IGuidIdentifiableEntity
    {
        Guid Id { get; set; } 
    }
}
