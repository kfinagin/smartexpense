﻿namespace SmartExpense.Domain.Classes
{
    using System;

    public class PaymentTag
    {
        public Guid PaymentId { get; set; }
        public Payment Payment { get; set; }

        public Guid TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
