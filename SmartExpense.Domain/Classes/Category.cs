﻿namespace SmartExpense.Domain.Classes
{
    using System;
    using System.Collections.Generic;
    using SQLite.Net.Attributes;

    public class Category : IGuidIdentifiableEntity
    {
        public Category()
        {
            this.Subcategories = new HashSet<Category>();
            this.CategoryTags = new HashSet<CategoryTag>();
            this.Payments = new HashSet<Payment>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long UsageCount { get; set; }

        public string Color { get; set; }

        public string QuickKey { get; set; }

        public Guid? ParentCategoryId { get; set; }
        
        public Category ParentCategory { get; set; }
        
        public virtual ICollection<Category> Subcategories { get; private set; }

        public virtual ICollection<CategoryTag> CategoryTags { get; private set; }

        public virtual ICollection<Payment> Payments { get; private set; }
    }   
}
