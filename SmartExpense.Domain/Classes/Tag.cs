﻿namespace SmartExpense.Domain.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class Tag : IGuidIdentifiableEntity
    {
        public Tag()
        {
            this.CategoryTags = new HashSet<CategoryTag>();
            this.PaymentTags = new HashSet<PaymentTag>();
        }

        public Guid Id { get; set; }

        public string Contents { get; set; }
        
        public virtual ICollection<CategoryTag> CategoryTags { get; private set; }

        public virtual ICollection<PaymentTag> PaymentTags { get; private set; } 
    }
}
