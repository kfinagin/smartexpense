﻿namespace SmartExpense.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    public class Location : IGuidIdentifiableEntity
    {
        public Location()
        {
            this.Payments = new HashSet<Payment>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public virtual ICollection<Payment> Payments { get; private set; }
    }
}
