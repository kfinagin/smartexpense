﻿namespace SmartExpense.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    public class Payment : IGuidIdentifiableEntity
    {
        public Payment()
        {
            this.PaymentTags = new HashSet<PaymentTag>();
        }

        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public Guid LocationId { get; set; }
        public Location Location { get; set; }

        public Guid CurrencyId { get; set; }
        public Currency Currency { get; set; }

        public virtual ICollection<PaymentTag> PaymentTags { get; private set; }

        public string Description { get; set; }

        public DateTime DateTime { get; set; }
        public decimal Amount { get; set; }
    }
}
