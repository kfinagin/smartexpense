﻿namespace SmartExpense.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    public class Currency : IGuidIdentifiableEntity
    {
        public Currency()
        {
            this.Payments = new HashSet<Payment>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Symbol { get; set; }

        public virtual ICollection<Payment> Payments { get; private set; } 
    }
}
